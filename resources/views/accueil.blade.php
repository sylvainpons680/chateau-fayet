@extends('layouts.main')

@section('content')
<div class=" ab main-home">
        <div class="card home-content">
            <div class="card-header title-card ">
                 Historique
            </div>
            <div class="card-body">
                <p class="card-text informations">Situé dans le Tarn et Garonne (82170 FABAS), le château FAYET est une exploitation viticole familiale de producteur-récoltant depuis 1882.
                André FAYET achète en 1989 une propriété voisine, en 1998 la construction d’une cave adaptée à la production voit le jour, en 2005 Florian (Fils FAYET) s’installe et achète des vignes.​</p>
            </div>
        </div>

    <div class="card home-content">
        <div class="card-header title-card">
                Situation  – Terroir - Climat
        </div>
        <div class="card-body">
            <p class="card-text informations">
                    Sur les terrasses entre le Tarn et la Garonne (entre Toulouse et Montauban), dans le Sud-Ouest de la France, en région Midi-Pyrénées, dans l’appellation AOP FRONTON.

                    Ses vignobles d’une surface totale de 20 ha, évoluent sur un terroir d’argiles et de boulbènes, de terrefort et de graviers profonds.

                    Les étés sont chauds, les hivers relativement doux et nous bénéficions du vent d’Autan
            </p>
        </div>
    </div>


    <div class="card home-content">
            <div class="card-header title-card">
                    L’appellation AOC Fronton
            </div>
            <div class="card-body">
                <p class="card-text informations">
                        AOC depuis 1975, pour être reconnu dans cette appellation, le taux du cépage Négrette (Mavro : noir en Grec) originaire de chypre,
                            l’assemblage doit être supérieur ou égal à 50%.
                </p>
            </div>
        </div>


    <div class="card home-content">
            <div class="card-header title-card">
                    Répartition des cépages
            </div>
            <div class="card-body aoc">
                <p class="card-text informations cepage">
                        <p>Surface totale : 20  ha</p><br>
                        <p>Capacité de production : 1400 hl</p><br>
                        <p> densités de plantation sont de 4300 pieds/ha,</p><br>
                         et les vignes ont de 1 à 45 ans.
                        Les vendanges se font à </p> la machine
                </p>
            </div>
    </div>

    <div class="card home-content">
            <div class="card-header title-card">
                    Cépage
            </div>
            <div class="card-body cepage">
                <p class="card-text informations ">
                        <p>Syrah 04 ha 20 ar</p><br>
                        <p>Négrette 07 ha 60 ar</p><br>
                        <p>Cabernet franc 02 ha 63 ar</p><br>
                        <p>Cabernet Sauvignon 01 ha</p><br>
                        <p>Merlot 01 ha 23 ar</p><br>
                        <p>Tannat 1 ha 17 ar</p><br>
                        <p>Gamay N 01 ha</p><br>
                        <p>Sémillon 00 ha 17 ar</p><br>
                        <p>Sauvignon 00 ha 04 ar</p><br>
                        <p>Chardonnay 00 ha 06 ar</p><br>
                        <p>Muscat de  Hambourg 00 ha 50 ar</p><br>
                </p>
            </div>
    </div>

</div>

@endsection
