@extends('layouts.main')
@section('content')
<h5 class="card-title nom"> {{$wine->nom}}</h5>

    <div class="description-wines">



        <div class="container img-desc">

                <img class="card-img-top " src="{{$wine->image_url}}" alt="photo vin">
        </div>
        <div class="description">

            <div class="card-body">
                <div class="cat"><p>Cépage:</p>
                    <p class="card-title">Cépage:{{$wine->description}}</p></div>
                <div class="cat"><p>Nom:</p>
                    <p class="card-title">Degrés d'alcool:{{$wine->degres}}</p></div>
                <div class="cat"><p>Nom:</p>
                    <p class="card-title">Terroir:{{$wine->terroir}}</p></div>
                <div class="cat"><p>Nom:</p>
                    <p class="card-title">Vinification:{{$wine->vinification}}</p></div>
                <div class="cat"><p>Nom:</p>
                    <p class="card-title">Notes de dégustation:{{$wine->notes_degusation}}</p></div>
                <div class="cat"><p>Nom:</p>
                    <p class="card-title">Accord:{{$wine->accord}}</p></div>

                    <a href="/commander" class="btn btn-primary">Commander</a>
                    <h6 class="card-title">{{$wine->prix}}</h6>

            </div>
        </div>

    </div>

@endsection

