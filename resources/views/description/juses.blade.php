@extends('layouts.main')
@section('content')
<h5 class="card-title nom"> {{$jus->name}}</h5>

    <div class="description-wines">



        <div class="container img-desc">

                <img class="card-img-top " src="{{$jus->image_url}}" alt="photo vin">
        </div>
        <div class="description">

            <div class="card-body">
                <div class="cat"><p>Cépage:</p>
                    <p class="card-title">Cépage:{{$jus->description}}</p></div>
                <div class="cat"><p>Nom:</p>
                    <p class="card-title">Degrés d'alcool:{{$jus->degres}}</p></div>
                <div class="cat"><p>Nom:</p>
                    <p class="card-title">Vinification:{{$jus->vinification}}</p></div>

                    <a href="/commander" class="btn btn-primary">Commander</a>
                    <h6 class="card-title">{{$jus->prix}}€</h6>

            </div>
        </div>

    </div>

@endsection

