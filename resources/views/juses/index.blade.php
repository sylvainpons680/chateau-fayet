@extends('layouts.main')
@section('content')
{{-- changer le css --}}
<div class="allcontent">

        <div class="col-sm-5">
            <h1 class="display-3">Jus</h1>
            <div>
                <a href="{{ route('juses.create')}}" class="btn btn-primary">Nouveau Jus</a>
            </div>
            <div>
                <a href='{{ route('admin')}}' class="btn btn-primary">Retour à la page d'aministration</a>
            </div>
        </div>

        <div class="allwine">

                @foreach($juses as $jus)

                <div class="wine_img">
                    <div class="card">

                        <div class="wineimg">

                            <img class="card-img-top" src="{{$jus->image_url}}" alt="Card image cap">
                        </div>

                        <div class="card-body">

                            <h5 class="card-title">Nom</h5>
                            <p class="card-text">{{$jus->name}}</p>

                            <a href="{{ route('juses.edit',["jus"=>$jus->id])}}" class="btn btn-primary">Modifier</a>

                            <form action="{{ route('juses.destroy',["jus"=>$jus->id])}}" method="post">
                                    @csrf
                                    @method('DELETE')
                                    <button class="btn btn-danger send" type="submit">Supprimer</button>
                                </form>

                        </div>
                    </div>
                </div>
                @endforeach
        </div>

            <div class="col-sm-12">

                @if(session()->get('success'))
                <div class="alert alert-success">
                    {{ session()->get('success') }}
                </div>
                @endif
            </div>

</div>


@endsection

