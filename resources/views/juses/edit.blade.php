@extends('layouts.main')
@section('content')
<div class="row">
        <div class="col-sm-8 offset-sm-2">
            <h1 class="display-3">Modifier un jus</h1>
            <div>
                <a href='{{ route('juses.index')}}' class="btn btn-primary">Retour</a>
            </div>

            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            <br />
            @endif
            <form method="post" action="{{ route('juses.update',["jus"=>$jus->id])}}">
                @method('PATCH')
                @csrf
                <div class="form-group">

                    <label for="name">Nom:</label>
                    <input type="text" class="form-control" name="name" value={{ $jus->name }} />
                </div>

                <div class="form-group">
                    <label for="description">Cépage:</label>
                    <input type="text" class="form-control" name="description" value={{ $jus->description }} />
                </div>

                <div class="form-group">
                    <label for="degres">Degres:</label>
                    <input type="text" class="form-control" name="degres" value={{ $jus->degres }} />
                </div>

                <div class="form-group">
                    <label for="vinification">Vinification:</label>
                    <input type="text" class="form-control" name="vinification" value={{$jus->vinification }} />
                </div>

                <div class="form-group">
                    <label for="prix">Prix:</label>
                    <input type="text" class="form-control" name="prix" value={{ $jus->prix }} />
                </div>

                <div class="form-group">
                    <label for="image_url">Photo:</label>
                    <input type="text" class="form-control" name="image_url" value={{$jus->image_url }} />
                </div>
                <button type="submit" class="btn btn-primary">Update</button>
            </form>
        </div>
    </div>
@endsection
