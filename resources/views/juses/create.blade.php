@extends('layouts.main')
@section('content')
<div class="row">
    <div class="col-sm-8 offset-sm-2">
       <h1 class="display-3">Ajouter un jus</h1>
       <div>
            <a href='{{ route('juses.index')}}' class="btn btn-primary">Retour a la liste des jus</a>
        </div>
     <div>
            @if ($errors->any())
            <div class="alert alert-danger">
              <ul>
                  @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                  @endforeach
              </ul>
            </div><br />
          @endif
         <form method="post" action="{{ route ('juses.store') }}">
             @csrf
             <div class="form-group">
                 <label for="name">Nom:</label>
                 <input type="text" class="form-control" name="name"/>
             </div>

             <div class="form-group">
                 <label for="description">Cépage:</label>
                 <input type="text" class="form-control" name="description"/>
             </div>

             <div class="form-group">
                 <label for="degre">Degres:</label>
                 <input type="text" class="form-control" name="degre"/>
             </div>

             <div class="form-group">
                 <label for="vinification">vinification:</label>
                 <input type="text" class="form-control" name="vinification"/>
             </div>

             <div class="form-group">
                 <label for="prix">Prix</label>
                 <input type="text" class="form-control" name="prix"/>
             </div>

             <div class="form-group">
                <label for="image_irl">Photo:</label>
                <input type="text" class="form-control" name="image_url"/>
             </div>

             <button type="submit" class="btn btn-primary">Ajouter un jus</button>
         </form>

     </div>
     <div class="col-sm-12">

            @if(session()->get('success'))
              <div class="alert alert-success">
                {{ session()->get('success') }}
              </div>
            @endif
          </div>
   </div>
   </div>
@endsection
