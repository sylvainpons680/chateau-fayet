
@extends('layouts.main')
@section('content')
<p>Merci de nous retourner votre bon de commande accompagné de votre règlement à: </p>
    <p>    Château Fayet
        572 route de coulon
        82170 FABAS</p>
        ​
        <p>votre commande sera livrée sous 10 jours ouvrables environ, après réception de votre courrier</p>

        <a target="_blank" href="https://docs.wixstatic.com/ugd/bb78c7_6de70910d85241c19012ed7656ef22e4.pdf">Pour imprimer le bon de commande :</a>

@foreach ( $basket->items as $item )
<div>

    <p>{{$item->basketable->nom}}</p>

    <form action="{{url('/commander/delete', ["item"=>$item]) }}" method="get">

        <button type="submit" class="btn btn-danger">Supprimer

        </button>
    </form>

</div>


@endforeach

<div>
    <p>total de bouteille:</p>
    {{$basket->getTotalNumberOfItems()}}
</div>

@endsection
