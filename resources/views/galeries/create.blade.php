@extends('layouts.main')
@section('content')
<div class="row">
    <div class="col-sm-8 offset-sm-2">
       <h1 class="display-3">Ajouter une photo</h1>
       <div>
            <a href='{{ route('galeries.index')}}' class="btn btn-primary">Retour a la liste des photos</a>
        </div>
     <div>
            @if ($errors->any())
            <div class="alert alert-danger">
              <ul>
                  @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                  @endforeach
              </ul>
            </div><br />
          @endif
         <form method="post" action="{{ route ('galeries.store') }}">
             @csrf
             <div class="form-group">
                 <label for="image_url">Photo:</label>
                 <input type="text" class="form-control" name="image_url"/>
             </div>

             <div class="col-sm-12">

                    @if(session()->get('success'))
                      <div class="alert alert-success">
                        {{ session()->get('success') }}
                      </div>
                    @endif
                  </div>

             <div class="form-group">
                <label for="created_at">Ajouter a :</label>
                <input type="time" class="form-control" name="created_at"/>
            </div>
             <button type="submit" class="btn btn-primary">Ajouter une photo</button>
         </form>

     </div>
   </div>
   </div>
@endsection
