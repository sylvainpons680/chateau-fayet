@extends('layouts.main')
@section('content')
<div class="row">
        <div class="col-sm-8 offset-sm-2">
            <h1 class="display-3">Modifier une Photo</h1>
            <div>
                <a href='{{ route('galeries.index')}}' class="btn btn-primary">Retour</a>
            </div>

            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            <br />
            @endif
            <form method="post" action="{{ route('galeries.update',["galerie"=>$galerie->id])}}">
                @method('PATCH')
                @csrf
                <div class="form-group">

                    <label for="image_url">Photo:</label>
                    <input type="text" class="form-control" name="image_url" value={{ $galerie->image_url }} />
                </div>

                <div class="form-group">
                        <label for="created_at">Ajouter a :</label>
                        <input type="time" class="form-control" name="created_at"/>
                    </div>

                <button type="submit" class="btn btn-primary">Modifier</button>
            </form>
        </div>
    </div>
@endsection
