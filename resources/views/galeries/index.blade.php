@extends('layouts.main')
@section('content')
        <div class="col-sm-5">
          <h1 class="display-3">Galerie</h1>
          <div>
              <a href="{{ route('galeries.create')}}" class="btn btn-primary">Nouvelle Photo</a>
          </div>
          <div>
              <a href='{{ route('admin')}}' class="btn btn-primary">Retour à la page d'aministration</a>
          </div>
        </div>
              @foreach($galeries as $galerie)

                <div class="galerie_img">
                    <div class="card">

                        <div class="images_galerie">

                            <img class="card-img-top" src="{{$galerie->image_url}}" alt="Card image cap">
                        </div>

                        <div class="card-body">


                            <a href="{{ route('galeries.edit',["galerie"=>$galerie->id])}}" class="btn btn-primary">Modifier</a>

                            <form action="{{ route('galeries.destroy',["galerie"=>$galerie->id])}}" method="post">
                                    @csrf
                                    @method('DELETE')
                                    <button class="btn btn-danger send" type="submit">Supprimer</button>
                                </form>

                        </div>
                    </div>
                </div>
               @endforeach


          <div class="col-sm-12">

                @if(session()->get('success'))
                  <div class="alert alert-success">
                    {{ session()->get('success') }}
                  </div>
                @endif
              </div>
@endsection
