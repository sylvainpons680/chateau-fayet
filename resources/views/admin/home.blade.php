@extends('layouts.main')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Panel D'administration</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in!
                </div>
            </div>
            <div>
                <a href='{{ route('wines.index')}}' class="btn btn-primary">Page d'administation des vins</a>
            </div>
            <div>
                <a href='{{ route('juses.index')}}' class="btn btn-primary">Page d'administation des jus</a>
            </div>
            <div>
                <a href='{{ route('galeries.index')}}' class="btn btn-primary">Page d'administation des photos</a>
            </div>
        </div>
    </div>
</div>
@endsection
