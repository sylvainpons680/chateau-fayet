@extends('layouts.main')

@section('content')
<div class="main-contact">

        <h2>Nos coordonnées:</h2>

            <hr class="ligne">

    <div class="all-contact">
        <div class="container">
            <p>Château FAYET</p>
                <p> 572 route de coulon</p>
                <p> 82 170 Fabas</p><br>

                <p>​Tél: Florian 06 48 77 46 35</p>
                <p>André 06 10 18 34 58</p>
                <p> Courriel : chateaufayet@hotmail.fr</p><br>

        </div>
        <div class="container horaire">


            <p>Horaires d'ouverture de la cave,</p><br>
            <p> Mercredi et Vendredi de 17h30 à 19h</p><br>
            <p> ou sur RDV du lundi au samedi</p>

        </div>
        </div>

        <div class="col-sm-12">
                @if(session()->get('success'))

                    <div class="alert alert-success">
                        {{ session()->get('success') }}
                    </div>

                @endif
            </div>

        <hr class="trait">
        <h1>Contactez-nous:</h1>
        <hr class="ligne">
<form action="contact" class="col-5 mailmarche" method="POST">
        @csrf

        <div class="form-group ">
            <input type="text" class="form-control @error('name') is-invalid @enderror" name="name"
            placeholder="Rentrez votre Nom ..." value="{{ old('name')}}">

            @error('name')
            <div class="invalid-feedback">
                {{$errors->first('name')}}
            </div>
            @enderror

        </div>

        <div class="form-group">

            <input type="email" class="form-control @error('email') is-invalid @enderror" name="email"
                placeholder="Rentrez un email ..." value="{{old('email')}}">

                @error('emain')
                <div class="invalid-feedback">
                    {{$errors->first('email')}}
                </div>
                @enderror
        </div>

        <div class="form-group">
            <textarea name="message" cols="30" rows="10" class="form-control @error('message') is-invalid @enderror"
            placeholder="Votre message">{{old ('message') }}</textarea>

                @error('message')
                <div class="invalid-feedback">
                    {{$errors->first('message')}}
                </div>
                @enderror
        </div>

    <button type="submit" class="btn btn-primary">Envoyer</button>
</form>
<hr class="trait-contact">
            <div class="maps">
                <h3>Situtation géographique:</h3>
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d23013.572076504!2d1.3233194566201587!3d43.86211870592363!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x12ac049c4a3fe4d7%3A0x406f69c2f3bd950!2s82170+Fabas!5e0!3m2!1sfr!2sfr!4v1561386532884!5m2!1sfr!2sfr" width="750px" height="290px"></iframe>
            </div>


@endsection
