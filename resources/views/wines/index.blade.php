@extends('layouts.main')
@section('content')

<div class="allcontent">

        <div class="col-sm-5">
            <h1 class="display-3">Vins</h1>
            <div>
                <a href="{{ route('wines.create')}}" class="btn btn-primary">Nouveau vin</a>
            </div>
            <div>
                <a href='{{ route('admin')}}' class="btn btn-primary">Retour à la page d'aministration</a>
            </div>
        </div>

        <div class="allwine">

                @foreach($wines as $wine)

                <div class="wine_img">
                    <div class="card">

                        <div class="wineimg">
                        <img type="file" class="card-img-top" src="{{$wine->image_url}}" alt="">
                            {{-- <img class="card-img-top" src="{{$wine->image_url}}" alt="Card image cap"> --}}
                        </div>

                        <div class="card-body">

                            <h5 class="card-title">Nom</h5>
                            <p class="card-text">{{$wine->nom}}</p>

                            <a href="{{ route('wines.edit',["wine"=>$wine->id])}}" class="btn btn-primary">Modifier</a>

                            <form action="{{ route('wines.destroy',["wine"=>$wine->id])}}" method="post" enctype="multipart/form-data">
                                    @csrf
                                    @method('DELETE')
                                    <button class="btn btn-danger send" type="submit">Supprimer</button>
                                </form>

                        </div>
                    </div>
                </div>
                @endforeach
        </div>

            <div class="col-sm-12">

                @if(session()->get('success'))
                <div class="alert alert-success">
                    {{ session()->get('success') }}
                </div>
                @endif
            </div>

</div>


@endsection

