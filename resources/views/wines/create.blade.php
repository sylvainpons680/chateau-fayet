@extends('layouts.main')
@section('content')
<div class="row">
    <div class="col-sm-8 offset-sm-2">
       <h1 class="display-3">Ajouter un vin</h1>
       <div>
            <a href='{{ route('wines.index')}}' class="btn btn-primary">Retour</a>
        </div>
     <div>
            @if ($errors->any())
            <div class="alert alert-danger">
              <ul>
                  @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                  @endforeach
              </ul>
            </div><br />
          @endif
         <form method="post" action="{{ route ('wines.store') }}">
             @csrf
             <div class="form-group">
                 <label for="nom">Nom:</label>
                 <input type="text" class="form-control" name="nom"/>
             </div>

             <div class="form-group">
                 <label for="description">Cépage:</label>
                 <input type="text" class="form-control" name="description"/>
             </div>

             <div class="form-group">
                 <label for="prix">Prix</label>
                 <input type="text" class="form-control" name="prix"/>
             </div>

             <div class="form-group">
                 <label for="degres">Degres:</label>
                 <input type="text" class="form-control" name="degres"/>
             </div>
             <div class="form-group">
                 <label for="notes_degustation">Note de dégustation:</label>
                 <input type="text" class="form-control" name="notes_degustation"/>
             </div>

             <div class="form-group">
                <label for="terroir">Terroir:</label>
                <input type="text" class="form-control" name="terroir"/>
            </div>

            <div class="form-group">
                <label for="vinification">vinification:</label>
                <input type="text" class="form-control" name="vinification"/>
            </div>

            <div class="form-group">
                <label for="accord">Accord:</label>
                <input type="text" class="form-control" name="accord"/>
            </div>

            <div class="form-group">
                <label for="image_url">Photo:</label>
                <input type="text" class="form-control" name="image_url"/>
            </div>

             <button type="submit" class="btn btn-primary">Ajouter un vin</button>
         </form>

     </div>
     <div class="col-sm-12">

            @if(session()->get('success'))
              <div class="alert alert-success">
                {{ session()->get('success') }}
              </div>
            @endif
     </div>

   </div>
   </div>
@endsection
