@extends('layouts.main')
@section('content')
<div class="row">
        <div class="col-sm-8 offset-sm-2">
            <h1 class="display-3">Modifier un vin</h1>
            <div>
                    <a href='{{ route('wines.index')}}' class="btn btn-primary">Retour</a>
                </div>

            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            <br />
            @endif
            <form method="post" action="{{ route('wines.update',['wine'=> $wine->id])}}" >
                @method('PATCH')
                @csrf
                <div class="form-group">

                    <label for="nom">Nom:</label>
                    <input type="text" class="form-control" name="nom" value="{{ $wine->nom }}{{ old('nom') }}" />
                </div>

                <div class="form-group">
                    <label for="description">Cépage:</label>
                    <input type="text" class="form-control" name="description" value="{{ $wine->description }}{{ old('description') }}" />
                </div>

                <div class="form-group">
                    <label for="prix">Prix:</label>
                    <input type="text" class="form-control" name="prix" value="{{ $wine->prix }}{{ old('prix') }}"/>
                </div>
                <div class="form-group">
                    <label for="degres">Degres:</label>
                    <input type="text" class="form-control" name="degres" value="{{ $wine->degres }}{{ old('degres') }} "/>
                </div>
                <div class="form-group">
                    <label for="notes_degustation">notes de dégustation:</label>
                    <input type="text" class="form-control" name="notes_degustation" value="{{ $wine->notes_degustation }}{{ old('notes_degustation') }}"/>
                </div>
                <div class="form-group">
                    <label for="terroir"> Terroir:</label>
                    <input type="text" class="form-control" name="terroir"  value="{{$wine->terroir }}{{ old('terroir') }}"/>
                </div>

                <div class="form-group">
                    <label for="vinification">Vinification:</label>
                    <input type="text" class="form-control" name="vinification" value="{{$wine->vinification }}{{ old('vinification') }}"/>
                </div>

                <div class="form-group">
                    <label for="accord">Accord:</label>
                    <input type="text" class="form-control" name="accord" value="{{$wine->accord }}{{ old('accord') }}" />
                </div>

                <div class="form-group">
                    <label for="image_url">Image:</label>
                    <input class="form-control" name="image_url" value="{{$wine->image_url }}{{ old('image_url') }}"/>
                </div>
                <button type="submit" class="btn btn-primary">Update</button>
            </form>
        </div>
    </div>
@endsection
