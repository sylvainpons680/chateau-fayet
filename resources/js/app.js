require("./main");
require("./bootstrap");

window.Vue = require("vue");

Vue.component(
    "head-component",
    require("./components/HeadComponent.vue").default
);
Vue.component(
    "galerie-component",
    require("./components/GalerieComponent.vue").default
);
Vue.component(
    "shop-component",
    require("./components/ShopComponent.vue").default
);
Vue.component(
    "basket-component",
    require("./components/BasketComponent.vue").default
);
Vue.component(
    "wine-component",
    require("./components/WineComponent.vue").default
);
Vue.component(
    "jus-component",
    require("./components/JusComponent.vue").default
);

const app = new Vue({
    el: "#app"
});
const picture = new Vue({
    el: "#galerie"
});
const shop = new Vue({
    el: "#shop"
});
const wine = new Vue({
    el: "#wine"
});
const Jus = new Vue({
    el: "#jus"
});
