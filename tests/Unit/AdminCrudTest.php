<?php

namespace Tests\Unit;


use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Jus;


class AdminCrudTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testCreate()
    {
        $jus = new Jus([
            "name" => "jus de pomme",
            "description" => "completer",
            "degre" => "0",
            "vinification" => "blah",
            "prix" => "10",
            "image_url" => "www.photo.com"
        ]);
        $jus->save();
    }

    public function testRead()
    { }
    public function testUpdate()
    { }
    public function testDelete()
    { }
}
