<?php

namespace App;


use Illuminate\Database\Eloquent\Model;
use DivineOmega\LaravelExtendableBasket\Interfaces\Basketable;

class Wine extends Model implements Basketable
{
    protected $fillable = [
        "id",
        "nom",
        "description",
        "prix",
        "degres",
        "notes_degustation",
        "terroir",
        "vinification",
        "accord",
        "image_url",
    ];
    protected $table = "wines";

    public function getPrice()
    {
        return $this->prix;
    }
    public function getName()
    {
        return $this->nom;
    }
    public function deleteitem()
    {
        $item = $this->items->first();
        $item->delete();
    }
    // For resolve the error (laravel unknow column 'updated_at')
    public $timestamps = false;
}
