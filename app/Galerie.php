<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Galerie extends Model
{
    protected $fillable= [
        "image_url",
        "created_at",
    ];
    // Pour le problème (laravel unknow column 'updated_at')
    public $timestamps = true;
}
