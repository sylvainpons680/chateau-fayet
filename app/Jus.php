<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DivineOmega\LaravelExtendableBasket\Interfaces\Basketable;

class Jus extends Model implements Basketable
{
    // Pour que Laravel accepte d'envoyer nos données à la base de données
    // For laravel accept to send data on the database
    protected $fillable = [
        "name",
        "description",
        "degre",
        "vinification",
        'prix',
        'image_url',
    ];
    public function getPrice(){
        return $this->prix;
    }
    public function getName(){
        return $this->name;
    }
    // Pour le problème (laravel unknow column 'updated_at')
    public $timestamps = false;
}
