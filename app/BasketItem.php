<?php

namespace App;

use Illuminate\Database\Eloquent\Relations\BelongsTo;
use DivineOmega\LaravelExtendableBasket\Models\BasketItem as BasketItemModel;

class BasketItem extends BasketItemModel
{
    public function basket(): BelongsTo
    {
        return $this->belongsTo('App\Basket');
    }
}
