<?php

namespace App\Http\Resources;


use Illuminate\Http\Resources\Json\JsonResource;

class GalerieResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [

            "image_url" => $this->image_url,
            "created_at" => $this->created_at,
        ];
    }
}
