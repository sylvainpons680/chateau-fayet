<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;


class WineRessource extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
        "id" => $this->id,
        "nom" => $this->nom,
        "description" => $this->description,
        "prix"=> $this->prix,
        "degres"=> $this->degres,
        "notes_degustation"=> $this->notes_degustation,
        "terroir"=> $this->terroir,
        "vinification"=> $this->vinification,
        "accord"=> $this->acoord,
        "image_url"=> $this->image_url,
        ];
    }
}
