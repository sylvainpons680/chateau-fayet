<?php

namespace App\Http\Resources;


use Illuminate\Http\Resources\Json\JsonResource;

class JusRessource extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id"=>$this->id,
            "name"=>$this->name,
            "description"=>$this->description,
            "degres"=>$this->degre,
            "vinification"=>$this->vinification,
            "prix"=>$this->prix,
            "image_url"=>$this->image_url,
        ];
    }
}
