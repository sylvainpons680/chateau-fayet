<?php

namespace App\Http\Controllers;


use App\Wine;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class NosVinsController extends Controller

{
    // public function __construct()
    // {
    //     $this->middleware('admin');
    // }

    public function vins()
    {
        $wines = Wine::all();

        return view('layouts.vin', ['wines' => $wines]);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('wines.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nom' => 'required',
            'description' => 'required',
            'prix' => 'required',
        ]);

        $wine = new Wine(
            $request->all()
        );
        $wine->save();
        return redirect('/wines')->with('success', 'Vin Ajouter!');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $wines = Wine::all();

        return view('wines.index', compact('wines'));
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('description.wines', ['wine' => Wine::findOrFail($id)]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $wine =  Wine::find($id);
        return view('wines.edit', compact('wine'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nom' => 'required',
            'description' => 'required',
            'prix' => 'required',
        ]);



        $wine = Wine::find($id);

        $wine->update($request->all());
        return redirect('/wines')->with('success', 'Vin modifier avec sucess!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $wine = Wine::find($id);
        $wine->delete();

        return redirect('/wines')->with('success', 'Vin supprimer!');
    }
}
