<?php

namespace App\Http\Controllers;
use App\Galerie;
use Illuminate\Http\Request;
use App\Http\Resources\GalerieResource;

class GalerieResourceController extends Controller
{
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $galeries= Galerie::get();

        return GalerieResource::collection($galeries);

    }

}
