<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Jus;

class NosJusController extends Controller
{


    public function jus()
    {
        $juses = Jus::all();
        //For used the var on the view
        return view('layouts.jus', ['juses' => $juses]);
    }

    /**
     * Display a listing of the resource.
     * Après avoir crée la function crée et remplir
     * l'index.blade.php
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $juses = Jus::all();
        return view('juses.index', compact('juses'));
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('juses.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
        ]);
        $jus = new Jus([
            'name' => $request->get('name'),
            'description' => $request->get('description'),
            'degre' => $request->get('degre'),
            'vinification' => $request->get('vinification'),
            'prix' => $request->get('prix'),
            'image_url' => $request->get('image_url')
        ]);
        $jus->save();
        return redirect('/juses/create')->with('success', 'Jus Ajouter');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('description.juses', ['jus' => Jus::findOrFail($id)]);
    }

    /**
     * Show the form for editing the specified resource.
     * Crée et remplir le fichier edit
     * et la function update
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $jus = Jus::find($id);
        return view('juses.edit', compact('jus'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required',
        ]);
        $jus = Jus::find($id);
        $request->all();
        // $jus->name = $request-> get('name');
        // $jus->description =$request->get('description');
        // $jus->degre = $request->get('degre');
        // $jus->vinification = $request->get('vinification');
        // $jus->prix = $request->get('prix');
        // $jus->image_url = $request->get('image_url');
        $jus->save();

        return redirect('/juses')->with('success', 'Jus modifier avec sucess!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $jus = Jus::find($id);
        $jus->delete();

        return redirect('/juses')->with('success', 'Jus supprimer');
    }
}
