<?php

namespace App\Http\Controllers;
use App\Wine;
use Illuminate\Http\Request;
use App\Http\Resources\WineRessource;

class WineRessourceController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $wines = Wine::get();

        return WineRessource::collection($wines);

    }
}
