<?php

namespace App\Http\Controllers;
use App\Jus;

use Illuminate\Http\Request;
use App\Http\Resources\JusRessource;

class JusRessourceController extends Controller
{
    public function index()
    {
        $juses = Jus::get();
        
        return JusRessource::collection($juses);
    }
}
