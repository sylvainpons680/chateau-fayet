<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Galerie;
use App\Http\Resources\GalerieResource;
class GalerieController extends Controller
{
   
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function galerie()
    {

        $galeries = Galerie::all();

        return view('layouts.galerie',['galeries'=> $galeries]);

    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('galeries.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'image_url'=>'required',
        ]);
        $galerie = new Galerie([
            'image_url'=>$request->get('image_url'),
            // 'created_at'=>$request->get('created_at')
        ]);
        $galerie->save();
        return redirect('galeries')->with('success', 'Photo ajouter ');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $galeries = Galerie::all();

        return view('galeries.index',compact('galeries'));
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $galerie = Galerie::find($id);
        return view('galeries.edit', compact('galerie'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'image_url'=>'required',

        ]);

        $galerie = Galerie::find($id );
        $galerie->image_url =  $request->get('image_url');
        // $galerie->created_at = $request->format('Y-m-d H:i:s');
        $galerie->save();

        return redirect('/galeries')->with('success', 'Vin modifier avec sucess!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $galerie = Galerie::find($id);
        $galerie->delete();
        return redirect('galeries')->with('success','Photo supprimer');
    }
}
