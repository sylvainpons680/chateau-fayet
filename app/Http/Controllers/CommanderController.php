<?php

namespace App\Http\Controllers;

use App\Jus;
use App\Wine;
use App\Basket;
use App\BasketItem;
use DivineOmega\LaravelExtendableBasket\Interfaces\Basketable;



class CommanderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('orders.commander', ['basket' => Basket::getCurrent()], ['wines' => Wine::all()]);
    }

    public function addwine($id)
    {
        $quantity = 1;
        $wine = Wine::find($id);


        Basket::getCurrent()->add($quantity, $wine);

        return redirect('/commander');
    }
    public function getitems()
    {


        $items = BasketItem::all();
        return view('orders.basket', ['wines' => Wine::all(), 'juses' => Jus::all(), 'basket' => Basket::getCurrent()]);
    }

    public function addjus($id)
    {
        // add 1 item on the basket
        $quantity = 1;
        // find the id jus
        $jus = Jus::Find($id);

        Basket::getCurrent()->add($quantity, $jus);
        return redirect('/commander');
    }
    public function deleteitem()
    {
        //avec get current on recupère ce qu'il y as dans le panier
        $basket = Basket::getCurrent();
        // on récupère l'item a supprimer
        $item = $basket->items->first();
        //on supprime l'item
        $item->delete();

        return redirect('/commander/basket');
    }
}
