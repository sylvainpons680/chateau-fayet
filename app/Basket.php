<?php

namespace App;

use Illuminate\Database\Eloquent\Relations\HasMany;
use DivineOmega\LaravelExtendableBasket\Models\Basket as BasketModel;


class Basket extends BasketModel
{
    public function items(): HasMany
    {
        return $this->hasMany('App\BasketItem');
    }
}
