<?php

use Illuminate\Database\Seeder;

class WinesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('wines')->insert([
            [
                'nom' => 'Rouge tradition',
                'description' => '50% Negrette  30% Syrah  20% Cabernet Franc  AOC Fronton',
                'prix' => '9.50',
                'image_url' => 'https://image.noelshack.com/fichiers/2019/44/1/1572262469-1567677814-tradition.png',
                'degres' => '12.5% vol  Bouteille 0.75 l',
                'notes_degustation' => 'Œil : rouge vif.  Nez : fruité.  Bouche : vin équilibré avec un milieu de bouche fruité,structuré et une belle présence en final',
                'terroir' => 'Boulbène / Terrefort  Densité de plantation : 4300 pieds/ha  Age moyen : 25 ans  Rendement à l’ha : 50 hl  Superficie de production : 8 ha.',
                'vinification' => 'Vendanges tardives à maturitées optimales.  Egrappées et foulées. Vignification classique 8 à 12 jours.  Fermentation à basse température autour de 25°.',
                'accord' => 'Servi à 16-18° excellent avec les plats du Sud Ouest, salade composée, viande blanche et poissons grillés.     A consommer dès aujourd’hui, peut vieillir 5 an'
            ],
            [
                'nom' => 'Rouge fût de chêne',
                'description' => '50% Negrette, 25% Syrah,25% Cabernet Sauvignon,AOC Fronton',
                'image_url' => 'https://image.noelshack.com/fichiers/2019/36/4/1567677895-rouge-fut-de-chene.png',
                'prix' => '9.50',
                'degres' => '12.5% vol  Bouteille 0.75 l',
                'notes_degustation' => 'Œil : rouge vif.

                Nez : structuré et boisé vanillé.

                Bouche : son boisé très fin se mêle aux arômes et aux tanins sans le dominer. Vin équilibré entre les tanins et la structure avec une belle longueur en final très agréable.',
                'terroir' => 'Densité de plantation : 4300 pieds/ha,Age moyen : 28 ans Rendement à l’ha : 45 hl Superficie de production : 3 ha',
                'vinification' => 'Vendange tardive à maturité optimale.Égrappé, foulé, fermentation longue entre 10 et 17 jours avec une extraction optimale (structure, couleur) et une température entre 25 et 28°.',
                'accord' => 'Servi à 16-18° excellent avec magret, confit de canard, viande de bœuf ainsi que le gibier.A consommer dès aujourd’hui éventuellement carafé, peut vieillir 7 ans',
            ],
            [
                'nom' => 'Rosé',
                'description' => '70% Négrette  30% Syrah  AOC Fronton',
                'prix' => '6.50',
                'image_url' => 'https://image.noelshack.com/fichiers/2019/36/4/1567677995-rose.png',
                'degres' => '12.5% vol  Bouteille 0.75 l',
                'notes_degustation' => 'Œil : rose pâle.  Nez : fruité et expressif.  Bouche : fruité agrumes, belle présence soyeuse en bouche.',
                'terroir' => 'Boulbènes/terrefort  Densité de plantation : 4300 pieds/ha  Age moyen : 15 ans  Rendement à l’ha : 60 hl  Superficie de production : 3 ha',
                'vinification' => 'Vendanges à maturitées.  Rosé de saignée.',
                'accord' => 'Servi à 6-8° excellent à l’apéritif, charcuterie fine, barbecue en famille ou entre amis, cuisine exotique et asiatique.  A ne pas manquer avec le fromage à pâtes persillées ainsi que les fromages de chèvre.     A consommer dès aujourd’hui, peut vieillir 2 ans',
            ],
            [
                'nom' => 'Blanc doux',
                'description' => '100% Sémillon  Vin de Pays du Comté Tolosan',
                'prix' => '7.50',
                'image_url' => 'https://image.noelshack.com/fichiers/2019/36/4/1567678174-blanc-doux.png',
                'degres' => '12% vol  Bouteille 0.75 l',
                'notes_degustation' => 'Œil : robe cristalline de couleur jaune clair.  Nez : fruité.  Bouche : structure soyeuse très fruité agréable en bouche voir gouleyant, une belle longueur en final.',
                'terroir' => 'Graviers  Densité de plantation : 4300 pieds/ha  Age moyen : 15 ans  Rendement à l’ha : 60 hl  Superficie de production : 30 ares',
                'vinification' => 'Vendanges tardives et fraiches (5-10°).  Pressurage faible et débourbage à froid (3°)  Fermentation à basse température (maxi 18°). Filtration',
                'accord' => 'Servi à 6° excellent à l’apéritif, avec le foie gras et les desserts.  A ne pas manquer avec le fromage à pâtes persillées ainsi que les fromages de chèvre.     A consommer dès aujourd’hui, peut vieillir 3 ans',
            ],
            [
                'nom' => 'Blanc sec',
                'description' => '50% Chardonnay  50% Sauvignon blanc  Vin de Pays du Comté Tolosan',
                'prix' => '6.50',
                'image_url' => 'https://image.noelshack.com/fichiers/2019/36/4/1567678284-blanc-sec.png',
                'degres' => '12% vol  Bouteille 0.75 l',
                'notes_degustation' => 'Œil : robe cristalline de couleur claire.  Nez : fruité.  Bouche : structure soyeuse, agrumes, sensation de fraîcheur en final.',
                'terroir' => 'Boulbènes  Densité de plantation : 4300 pieds/ha  Age moyen : 18 ans  Rendement à l’ha : 60 hl  Superficie de production : 30 ares',
                'vinification' => 'Vendanges à maturitées et fraiches (5-10°).  Pressurage faible et débourbage à froid (3°)  Fermentation à basse température (maxi 18°). Filtration',
                'accord' => 'Servi à 8° excellent à l’apéritif.  A ne pas manquer avec les poissons les plus fins et les crustacés.     A consommer dès aujourd’hui, peut vieillir 3 ans',
            ]
        ]);
    }
}
