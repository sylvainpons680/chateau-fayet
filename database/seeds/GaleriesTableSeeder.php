<?php

use Illuminate\Database\Seeder;

class GaleriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('galeries')->insert([

            ['image_url'=> 'https://pbs.twimg.com/media/DB1JEVYV0AMapWW.jpg'],
            ['image_url'=>'https://usercontent2.hubstatic.com/14232871_f520.jpg'],
            ['image_url'=>'https://upload.wikimedia.org/wikipedia/commons/2/24/Kim_Tae-yeon_at_Incheon_Airport_on_January_11%2C_2019.jpg']
        ]);
    }
}
