<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWinesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wines', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nom')->nullable();
            $table->text('description')->nullable();
            $table->string('prix')->nullable();
            $table->string('image_url')->nullable();
            $table->string('degres')->nullable();
            $table->text('notes_degustation')->nullable();
            $table->string('terroir')->nullable();
            $table->string('vinification')->nullable();
            $table->text('accord')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wines');
    }
}
