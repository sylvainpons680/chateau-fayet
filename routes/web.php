<?php

use App\Http\Controllers\ContactController;
use Aimeos\Shop\Controller\CatalogController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//Front Routes
Route::get('/', 'AccueilController@index');
Route::get('/home', 'AccueilController@home');
Route::get('/nosvins', 'NosVinsController@vins');
Route::get('/nosjus', 'NosJusController@jus');
Route::get('/commander', 'CommanderController@index');
Route::get('/galerie', 'GalerieController@galerie');


// Admin  Routes
Route::get('/admin', 'AdminController@index')->name('admin');


Auth::routes();


//Crud Routes
Route::resource('wines', 'NosVinsController')->middleware('admin');
Route::resource('juses', 'NosJusController')->middleware('admin');
Route::resource('galeries', 'GalerieController')->middleware('admin');


//Contact Routes

Route::get('contact', 'ContactController@create');
Route::post('contact', 'ContactController@store');

//Description wines/jus routes
Route::get('description/{id}', 'NosVinsController@show');
Route::get('descriptions/{id}', 'NosJusController@show');

//Route resource
Route::get('/galerie/description', 'GalerieResourceController@index');
Route::get('/wine/resource', 'WineRessourceController@index');
Route::get('/jus/resource', 'JusRessourceController@index');


//route commander
Route::get('/commander/addwine/{id}', 'CommanderController@addwine');
Route::get('/commander/basket', 'CommanderController@getitems');
Route::get('/commander/addjus/{id}', 'CommanderController@addjus');
Route::get('/commander/delete/{id}', 'CommanderController@deleteitem');
Route::get('/commander/basket/current', 'CommanderController@deleteitem');

//test
Route::get('/horizontal', "HorizontalController@index");
